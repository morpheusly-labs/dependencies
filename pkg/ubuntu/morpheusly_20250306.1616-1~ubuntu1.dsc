-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: morpheusly
Binary: morpheusly
Architecture: any
Version: 20250306.1616-1~ubuntu1
Maintainer: Morpheusly <morpheusly@morpheusly.com>
Homepage: https://morpheusly.com
Standards-Version: 4.7.2
Build-Depends: debhelper-compat (= 13), dh-sequence-single-binary, clang, cmake, curl, git, libayatana-appindicator3-dev, libsodium-dev, libstdc++-9-dev, morpheusly-venv-agent-test, mrph-flutter (>> 3.24.5), mrph-flutter (<< 3.24.6~), ninja-build, pkg-config, python3-yaml, python3.11-dev, unzip
Package-List:
 morpheusly deb comm optional arch=any
Checksums-Sha1:
 89f1b37b24660a5b19187f96e2d2e993abaad4e0 644624 morpheusly_20250306.1616.orig.tar.xz
 789511ac6482e5ce542c2097696ce0a1e97ca4a4 8352 morpheusly_20250306.1616-1~ubuntu1.debian.tar.xz
Checksums-Sha256:
 5fcb360e05466350519f3ac25338415fb33088447c621c8254e6702c714242b7 644624 morpheusly_20250306.1616.orig.tar.xz
 11ec498b4205f34d86c0d9958f89a0f168502edf4a8da0a2c99422624449c4d9 8352 morpheusly_20250306.1616-1~ubuntu1.debian.tar.xz
Files:
 46aa8b82529ddf4c8d604ef42caaf8eb 644624 morpheusly_20250306.1616.orig.tar.xz
 33c71ea3a854691e9b469bc41832f7d1 8352 morpheusly_20250306.1616-1~ubuntu1.debian.tar.xz


-----BEGIN PGP SIGNATURE-----

wsE7BAEBCgBvBYJnycqrCRBZ7trEIbzlCkcUAAAAAAAeACBzYWx0QG5vdGF0aW9u
cy5zZXF1b2lhLXBncC5vcme/iOEGqqczFbTj1he2lFDU2QvuzFD9oK0KFD4UxHGW
7BYhBPB2wVHYadQHnRFw21nu2sQhvOUKAAC/FQwAo1eiokRvfYjRRJ2kqM763bKf
+TlN1MrTYFRYV9dS7qqp43HBL1K8FJC2qkazmnoz4tTdCsyMbEhko7GLBng90pIF
yLzUoCpOV4P8SGOv0cRpt459UfF4zwg5wv58Uo+DK5BHCWOarTZmHjXTCWUQg3IJ
p/tzkZIDqIAsUEa/gr2nLNnoy35naqWhEhJ4JyYRA47sn/brJE6V4l67IGgroLTe
yOrKnMDycqhelKOoJjVd66BetDaPxala6xOKwncTABSb04DZbH338eMRW8sGGlIh
RNWsQse4YcX8MiinTMBx+2Gf8gYM6wxTKznieGv37kIySD28pABAykfCGBFDtGS4
IUnM0SpUWCeHBqSsMmYDasFLEdgWsynBOVB10Qc/tpXso1EiBEm1OkBWdh5rc7/D
MxQe8tHlisUkvEVpAZEvBGxJAcbQb9UHq9sYrTJJVs5q5StXagiTnVufmdPWTlbZ
WX2RXWy871CtxEK7m25KlVDwIbFNZ6rml9HjYJLr
=kwL/
-----END PGP SIGNATURE-----
