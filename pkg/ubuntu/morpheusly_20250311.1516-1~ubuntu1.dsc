-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: morpheusly
Binary: morpheusly
Architecture: any
Version: 20250311.1516-1~ubuntu1
Maintainer: Morpheusly <morpheusly@morpheusly.com>
Homepage: https://morpheusly.com
Standards-Version: 4.7.2
Build-Depends: debhelper-compat (= 13), dh-sequence-single-binary, clang, cmake, curl, git, libayatana-appindicator3-dev, libsodium-dev, libstdc++-9-dev, morpheusly-venv-agent-test, mrph-flutter (>> 3.24.5.4), mrph-flutter (<< 3.24.6~), ninja-build, pkg-config, python3-yaml, python3.11-dev, unzip
Package-List:
 morpheusly deb comm optional arch=any
Checksums-Sha1:
 4ee7e2e3532995f3e7a81cfe3bfb2fdf5534e609 646524 morpheusly_20250311.1516.orig.tar.xz
 e5e03c773727dc959b49dcb5c9582724133262fc 8452 morpheusly_20250311.1516-1~ubuntu1.debian.tar.xz
Checksums-Sha256:
 bc6d28da7e881396bd9970a4c03cf5644faa73809333f49a3a68811bd15264ed 646524 morpheusly_20250311.1516.orig.tar.xz
 ecb3c65acfd27c76a503817ebc1d245b16e5d25f212f17a464e585f6b2c23397 8452 morpheusly_20250311.1516-1~ubuntu1.debian.tar.xz
Files:
 cfb6a93e63228371886c5b7fac7d2ddf 646524 morpheusly_20250311.1516.orig.tar.xz
 065b7c541995e9e1b27a959e00b947f7 8452 morpheusly_20250311.1516-1~ubuntu1.debian.tar.xz


-----BEGIN PGP SIGNATURE-----

wsE7BAEBCgBvBYJn0FQmCRBZ7trEIbzlCkcUAAAAAAAeACBzYWx0QG5vdGF0aW9u
cy5zZXF1b2lhLXBncC5vcme2rVc01O4Z2XVqnqXExqNLgucPAr3ri2nGlrHfpNcL
ChYhBPB2wVHYadQHnRFw21nu2sQhvOUKAACtFwwAkXu9H6fDPdoWIDr5k6xo4KH5
PxmYEMTSBA4vWO8OaqJ3r5TUVcQPqRsAIwAes16q4hd69JHR/B4x1sh1JPhJ1UfC
zJD8hkJfzAx8xf+HtKskWHPrlCcphSjTcJrff34XECCHkw6MOO3DZElKB39swOch
P1DfaaI5C+p9HnOhI5FR9Nh2kRm51JZz8Y5hwUat6rReuaT5d3BjS+DiUS03VS+O
krykAi17T+qykcx/i3lgsNU/kY6k+FkqF9KaH3Hgj3l1zBdI1h6qRutOwRkch5L9
9tPAY01A8T796wDoAHA4td6gzWpiQ42KxSQX2oeO2ts2XRJBrFWjdGciX76xV3rK
w8HrgCUnF41azhOBTgJQYP+4AhKPQnLF8iSGeSFub94yAYNVz205HVURwUmMz9Vw
o9Qy2dV7TFnxUnGtUL6RL3tZbJIB9vFnp2TUQtOPTvN82q9yZw3ooRiywc37QCHm
IjaNtxfu6Aie9zzDc8J33BJxS9P1iGUClIa262Ih
=eKTU
-----END PGP SIGNATURE-----
